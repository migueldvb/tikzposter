#!/usr/bin/python
from string import Template
import subprocess

with open('theme_testing.tex', 'r') as f:
  template = Template(f.read())

tokens = 'Default', 'Rays', 'Basic', 'Simple', 'Envelope', 'Wave', 'Board', 'Autumn', 'Desert'


for token in tokens:
    page = template.substitute(
        {
            'themevar': token,
            }
        )
    with open(token + '.tex', 'w') as f:
        f.write(page)
    subprocess.call('pdflatex {}.tex'.format(token).split())
    subprocess.call('rm -f {}.tex'.format(token).split())
    subprocess.call('rm -f {}.aux'.format(token).split())
    subprocess.call('rm -f {}.log'.format(token).split())

subprocess.call(
    'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=themes.pdf'.split() +
    [t + '.pdf' for t in tokens]
)


for token in tokens:
    subprocess.call('rm -f {}.pdf'.format(token).split())
